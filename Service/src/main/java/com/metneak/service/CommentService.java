package com.metneak.service;

import com.metneak.repository.model.Comment;
import com.metneak.repository.model.CommentRepository;
import com.metneak.repository.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;



@Service
public class CommentService {
    @Autowired
    CommentRepository commentRepository;

    public boolean addComment(Comment comment) {
         return commentRepository.addComment(comment);
    }

    public Boolean updateComment(String detail,Integer id){
        return commentRepository.updateComment(detail, id);
    }

    public Boolean removeComment(Integer id){
        return commentRepository.removeComment( id);
    }

//    List<User>showUserCommentById(int id){
//        return commentRepository.showUserCommentById(id);
//    }

}
