package com.metneak.service;

import com.metneak.repository.UserRepository;
import com.metneak.repository.model.Group;
import com.metneak.repository.model.Location;
import com.metneak.repository.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {
    @Autowired
    UserRepository userRepository;

    public boolean insert(User user){
        if(userRepository.addNewUser(user)){
            return true;
        }
        return false;
    }

    public List<String> userNumber(){
        return userRepository.allUserNumber();
    }

    //user(id)
    public List<User>showUserById(int id){return userRepository.showUserById(id);}

    //location
    public Location showLocationById(int id){return  userRepository.showLocationById(id);}

    //user_groups
    public List<Group>showUserGroupById(int id){return userRepository.showUserGroupById(id);}

    public User findUser(String number) {
        return userRepository.findUserByPhone(number);
    }







}
