
//multiple parameter to generate comment , not yet check condition here
function generateComment(
  cmt_owner_name = "",
  comment_owner_image = "",
  text = ""
) {
  return `
  <div class="row">
    <div class="col-12">
      <img class="border border-danger user-img"
        src="${comment_owner_image}"
        alt="" srcset="">


      <p class="ml-2 d-inline-block comment-input p-3" style="border-radius:75px; border: 0px;background-color: rgba(0,0,0,.03);width: 80%; max-height: 70%;">
          <b>${cmt_owner_name} : </b>
          ${text}
          <br>

        </p>


    </div>

    <div class="flex-container" style="width:100%;">
      <a href=""></a>
        <a href="#">
            <p>Reply</p>
        </a>
        <a href="">
            <p>see more reply</p>
        </a>
        <a href=""></a>
    </div>

</div>
  `;
}

//multiple parameter to generate poster with comments and likes , not yet check condition of null object
//user img not work
function generatePost(
  post_owner_img,
  post_owner_name = "unknown",
  post_time = "10 min ago",
  detail = "say nothing",
  img,
  likes,
  comments
) {
    var className="row";

        if(img=="img/"){
            className="none";
            img=="img/person.jpg"
        }
  var cmtBlock = comment_Generator(comments);
  return `
 <div class="card shadow">

      <!-- end of post  -->
      <div class="card-header border-bottom-0" style="background-color: rgba(0,0,0,0) !important;">
        <br>
        <div class="container-fluid" ">
                <div class=" row">
          <!-- first part -->
          <div class="col-lg-11 col-sm-11">
            <img class="border border-danger user-img"
              src="${post_owner_img}"
              alt="" srcset="">
            <div class="first-line">


                <h5 class="ml-2">

                <b class="mt-5">${post_owner_name}</b>
                <br><sub class="text-info"> ${post_time}</sub></h5>
            </div>

          </div>
          <div class="col-lg-1 col-sm-1">
            <!-- id="myModal" class="modal fade" role="dialog" -->
            <a type="button" id="myModal" class="modal_btn" data-toggle="modal" data-target="#commentPopUp"><i class="fas fa-ellipsis-h"></i></a>
            <!-- <button href="#" id="commentPopUp" class="modal fade" role="dialog"><button> -->
          </div>
          <!-- first part -->
          <!-- second part post img and status -->
          <div class="col-lg-12">
            <div class="py-4">
              <h5 class="">
                ${detail}
              </h5>
            </div>
          </div>

          <!-- second part post img and status -->
        </div>
      </div>
    </div>
    <!-- end of post  -->


    <div class="container-fluid">
      <div class="row">
        <div class="col-12" style="display: ${className}">
          <img class="w-100 card-img-top shadow"
            src="${img}"
            alt="Card image">

        </div>
        <div class="col-12 mt-5 py-3 border border-right-0 border-left-0">
          <div class="ml-4">
            <i class="far fa-2x fa-heart" aria-hidden="true"></i>
            <span style="font-size:20px;" class="ml-2">Like</span>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <i class="far fa-2x fa-comment-alt " aria-hidden="true" style="color:black;"></i>
            <span style="font-size:20px;" class="ml-2">Comment</span>
          </div>
        </div>
      </div>
    </div>

    <div class="card-body">
      <div class="container-fluid">

        ${cmtBlock}
        <div class="row">
          <div class="col-12">
            <img class="border border-danger user-img  d-inline-block "
              src="${post_owner_img}"7
              alt="" srcset="">

            <input class="ml-2 d-inline-block comment-input p-3" placeholder="    say something .... !" type="text"
              style="width:78%; max-height: 70%; border-radius:75px; border: 0px; background-color: rgba(0,0,0,.03); " id="cmt">
                <input class="d-inline-block btn btn-success m-2"  type="button" value="Send" onclick="Comment()">
          </div>


        </div>

      </div>
      <!--  -->
    </div>
</div>`;
}
 //generate comment by comment objects parameter
            function comment_Generator(comments) {
              var result = " ";
              if (comments.length == 0) {
                return "";
              }

              for (i = 0; i < comments.length; i++) {
                var cmt = comments[i];
                var cmtDetail = cmt["detail"];
                var user_cmt = cmt["user"];
                var user_name_cmt = user_cmt["name"];
                var user_profile_cmt = user_cmt["image"];

                if (user_profile_cmt == null) {
                  user_profile_cmt = "IMG_3770.jpg";
                }

                if (cmt["parent_id"] == 0) {
                  //need img and name owner cmt
                  result =
                    result +
                    generateComment(user_name_cmt, "img/" + user_profile_cmt, cmtDetail);
                } else {
                  return " "; //for reply comment
                }
              }
              return result;
            }

 //to generate post by pass poster object ,
            function posterGenerate(poster) {

              var arrayPosters = poster["Data"];
              console.log(arrayPosters); //loop here to multiple posters

              for (var i = 0; i < arrayPosters.length; i++) {
                console.log(i);
                var Post = arrayPosters[i];

                var comments = Post["comments"]; //multi column
                var likes = Post["likes"]; //multi like

                var detail = Post["detail"];  //get post detial

                var group_id = Post["group_id"];//get group id that post to

                var post_id = Post["id"];    //post id

                var post_img = Post["image"];   //poster image

                var user_id = Post["user_id"];   //get owner id poster

                var owner_name = Post["user"];//get object user post owner

                var userImg = owner_name["image"] == null ? 'IMG_3770.jpg': owner_name["image"];   //if null image ser default

                var name = owner_name["name"];   //owner name

                //get posters
                document.getElementById("post").innerHTML += generatePost(
                  "img/" + userImg,
                  name,
                  "time",
                  detail,
                  "img/" + post_img,
                  likes,
                  comments
                );
              }
            }

