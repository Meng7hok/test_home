package com.metneak.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class FileUploadConfigure implements WebMvcConfigurer {
    @Value("UPLOAD_IMAGE")
    String serverPath;
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
//        registry.addResourceHandler("/img/**").addResourceLocations("file:///"+System.getProperty("user.dir")+"\\src\\main\\resources\\static\\user-img\\");
        registry.addResourceHandler("/img/**").addResourceLocations("file:Controller/src/main/resources/static/user-img/");
    }
}
//