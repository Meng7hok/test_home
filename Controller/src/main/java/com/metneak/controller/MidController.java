package com.metneak.controller;

import com.metneak.service.MetNeakService;
import com.metneak.repository.model.Group;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
public class MidController {


//    @RequestMapping("/home")
//    String home(){
//
//        return  "homepage";
//    }
    @RequestMapping("/res")
    @ResponseBody
    String register(){
        metNeakService.save();
        return  "register";
    }
    @RequestMapping("/index")
    @ResponseBody
    String index(){
        metNeakService.findGroup();
        return "index";
    }
    @Autowired
    MetNeakService metNeakService;

    @GetMapping("/list")
    @ResponseBody
    String list(){
        return metNeakService.list().toString();
    }

    @GetMapping("/testprocess")
    @ResponseBody
    String insertProcess(){
        List<Group > lists = new ArrayList<>();
        lists.add(new Group(2011,1,null,null));

        lists.add(new Group(2015,2,null,null));

        lists.add(new Group(2017,3,null,null));

        return "work aii";
    }


}
