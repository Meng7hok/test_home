package com.metneak.controller;

import com.metneak.repository.UserRepository;
import com.metneak.repository.model.Group;
import com.metneak.repository.model.Post;
import com.metneak.repository.model.User;
import com.metneak.service.GroupService;
import com.metneak.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

@Controller
public class TestController {
    @Autowired
    private UserRepository userRepository;

    private static Integer current_index=2;

    @RequestMapping("/setAttribute")
    String setter(HttpServletRequest request){
        User user1 = new User();
        user1.setName("tester");
        request.getSession().setAttribute("User",user1);
        return "redirect:/group";
    }

    @RequestMapping("/settAttribute")
    String seter(HttpServletRequest request){
        User user1 = new User();
        user1.setName("developer");
        request.getSession().setAttribute("User",user1);
        return "redirect:/group";
    }

    @Autowired
    GroupService groupService;

    @RequestMapping("/group")
    String home( HttpServletRequest request,ModelMap modelMap) {
//        User current_user = (User) request.getSession().getAttribute("User");
        User current_user = userRepository.findUserByPhone("098323232");
        current_user.setGroupBy(1,2,3);
        current_user.setImage("img/IMG_3770.jpg");
        List<Group> groups = groupService.findGroupDetail(current_user.getGroup());
        modelMap.addAttribute("GROUPS",groups);
        modelMap.addAttribute("USER",current_user);
        modelMap.addAttribute("CURRENT_INDEX",current_index);
//        System.out.println(current_index);
//        System.out.println(current_user.getName());
//        System.out.println(current_user.getCover());
//        System.out.println(current_user.getId());
//        System.out.println(groups.get(0).getYear());
//        System.out.println(groups.get(0).getSchool().getName());
        System.out.println(groups);

        return "groupnewsfeed";
//        return "testdata";
    }

    @RequestMapping("profile")
    String profile(HttpServletRequest request,ModelMap modelMap){
       //User is_user = (User)request.getSession().getAttribute("user");
        User is_user = userRepository.findUserByPhone("098323232");
        is_user.setImage("image/IMG_3770.jpg");
        is_user.setCover("image/IMG_3770.jpg");
        List<Group> groups = groupService.findGroupDetail(is_user.getGroup());
        modelMap.addAttribute("GROUPS",groups);
        modelMap.addAttribute("NAME",is_user.getName());
        modelMap.addAttribute("LOCATION",is_user.getLocation_id());
        System.out.println(is_user.getName());
        return "profile";
    }


    @RequestMapping("switch")
    String groupSwitcher(@RequestParam("index") Integer index){
        TestController.current_index = index;
        return "redirect:/group";

    }

    @PostMapping("file")
    @ResponseBody
    String imageTest(@RequestBody Map<String,Object> map){
//        System.out.println((Linh) map.get("image"));
        return "hehehehe";
    }

}
