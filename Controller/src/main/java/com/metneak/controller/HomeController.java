package com.metneak.controller;


import com.metneak.service.GroupService;
import com.metneak.service.UserService;
import com.metneak.repository.Session;
import com.metneak.repository.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;


import java.sql.Date;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;


@Controller
public class HomeController {
    @Autowired
    GroupService groupService;

    @Autowired
    UserService userService;

    @GetMapping("/a")
    String post(){
        return "index";
    }
    @GetMapping("/t")
    String pdost(){
        return "tetst";
    }
    @RequestMapping("/")
    String homepage() {
        return "homepage";
    }

    @RequestMapping("/personal")
    String personalInfo(ModelMap modelMap) {
        modelMap.addAttribute("user", "");
//          groupService.groupProcess(lists,newUser);
        return "PersonalInfoForm";
    }

    @RequestMapping("/school")
    String schoolInfo(ModelMap map)
    {
        map.addAttribute("SCHOOLS",groupService.allSchools());
        System.out.println(groupService.allSchools());
        return "SchoolInfoForm";
    }

    @RequestMapping("/home")
    String home() {

        return "groupnewsfeed";
    }



}
