package com.metneak.controller;

import com.metneak.repository.Session;
import com.metneak.repository.model.Group;
import com.metneak.repository.model.User;
import com.metneak.service.GroupService;
import com.metneak.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import java.sql.Date;
import java.time.LocalDate;
import java.util.*;

@Controller
public class UserController {

    @Autowired
    UserService userService;

    @Autowired
    GroupService groupService;

    RestTemplate restTemplate = new RestTemplate();

    private User newUser;


    @PostMapping("/school")
    @ResponseBody
    Map<String,Object> info(@RequestBody Map<String,Object> map,HttpServletRequest request){
        try {
            Map<String, Object> data = map;
            ArrayList<Integer> years = (ArrayList<Integer>) data.get("years");
            ArrayList<Integer> schools = (ArrayList<Integer>) data.get("schools");

            List<Group> lists = new ArrayList<>();
            lists.add(new Group(years.get(0), schools.get(0), null, null));
            lists.add(new Group(years.get(1), schools.get(1), null, null));
            lists.add(new Group(years.get(2), schools.get(2), null, null));

            groupService.groupProcess(lists ,newUser);
//            groupService.groupProcess(lists,new User());
            User user = userService.findUser(newUser.getPhone());
            request.getSession().setAttribute("user",user);

            map.put("redirect","/home");
        }catch (Exception e){
            e.printStackTrace();
            map.put("redirect","/school");
        }


        return map;

    }

    @PostMapping("/personalInfo")
    String getPersonalInfo(@RequestParam String name, @RequestParam String lastName, @RequestParam String gender, @RequestParam String bday) {

        LocalDate obj = LocalDate.of(Integer.parseInt(bday.split("/")[2]), Integer.parseInt(bday.split("/")[0]), Integer.parseInt(bday.split("/")[1]));
        newUser.setName(lastName + " " + name);
        newUser.setBirthdate(Date.valueOf(obj));
        newUser.setGender(gender);


        return "redirect:/school";
    }

    @PostMapping("/verified")
    public String getAdminPage(@ModelAttribute Session code, ModelMap modelMap, HttpServletRequest request) {

        Map<String, Object> obj = restTemplate.getForObject("https://graph.accountkit.com/v1.3/access_token?grant_type=authorization_code&code=" + code.getCode() + "&access_token=AA|1242809985886635|dfba99507657b5fab77eb2d5d26ab234", HashMap.class);
        String accessToken = (String) obj.get("access_token");
        Map<String, Object> get = restTemplate.getForObject("https://graph.accountkit.com/v1.3/me/?access_token=" + accessToken + "", HashMap.class);
        Map<String, Object> test = (Map<String, Object>) get.get("phone");

        User user =   userService.findUser(test.get("number").toString());

        if(user!=null){
            request.getSession().setAttribute("user",user);
            return "redirect:/home";
        }
//        for (String ph : userService.userNumber()) {
//            if (test.get("number").toString().equals(ph)) {
//                return "redirect:/home";
//            }
//        }
        newUser = new User();
        newUser.setPhone((String) test.get("number"));
        modelMap.addAttribute("phone", test.get("number"));
        return "redirect:/personal";
    }
}
