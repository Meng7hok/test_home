package com.metneak.restController;

import com.metneak.repository.model.User;
import com.metneak.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/")
@Api(value = "User Restful API", tags = {"v2/api/users"}, description = "user crud")
public class UserRestController {
    @Autowired
    public UserService userService;

    //SHOW USER BY ID
    @ApiOperation("TO DO: TO SHOW USR BY ID")
    @GetMapping("/user/{id}")
    public ResponseEntity<Map<String, Object>> showUserById(@PathVariable("id") int id) {
        Map<String, Object> response = new HashMap<>();
        List<User> user = userService.showUserById(id);
        if (user == null) {
            response.put("status", false);
            response.put("message", "User is not found.");
            return new ResponseEntity<>(response, HttpStatus.NOT_ACCEPTABLE);
        } else {
            response.put("status", true);
            response.put("message", "Successfully!");
            response.put("User", user);
            return new ResponseEntity<>(response, HttpStatus.OK);
        }
    }
}
