
        package com.metneak.restController;

import com.metneak.helper.GlobalFunctionHelper;
import com.metneak.service.GroupService;
import com.metneak.service.PostService;
import com.metneak.repository.model.Comment;
import com.metneak.repository.model.Post;
import com.metneak.repository.model.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.models.auth.In;
import org.apache.ibatis.annotations.ConstructorArgs;
import org.apache.ibatis.annotations.Delete;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/")
@Api(value = "Post Rest",tags ={"v2/api/posts"},description = "post crud")
public class PostRestController {

    @Autowired
    public PostService postService;

    //SHOW POST
    @ApiOperation("TO DO: TO SHOW ALL POSTS BY GROUP")
    @GetMapping("/posts")
    public ResponseEntity<Map<String, Object>> showAllPosts(@RequestBody Integer group_id) {
        Map<String, Object> response = new HashMap<>();
        List<Post> posts = postService.showAllPostsByGroup(group_id);
        if (posts.isEmpty()) {
            response.put("status", false);
            response.put("message", "That is no posts");
            return new ResponseEntity<>(response,HttpStatus.NOT_ACCEPTABLE);
        } else {
            response.put("status", true);
            response.put("message", "Successfully Retrieved Posts by GROUP!");
            response.put("Data", posts);
            return new ResponseEntity<>(response,HttpStatus.OK);
        }
    }

    //SHOW POST
    @ApiOperation("TO DO: TO SHOW ALL POSTS")
    @GetMapping("/post")
    public ResponseEntity<Map<String, Object>> showAllPosts() {
        Map<String, Object> response = new HashMap<>();
        List<Post> posts = postService.showAllPosts();
        if (posts.isEmpty()) {
            response.put("status", false);
            response.put("message", "That is no posts");
            return new ResponseEntity<>(response,HttpStatus.NOT_ACCEPTABLE);
        } else {
            response.put("status", true);
            response.put("message", "Successfully Retrieved Post!");
            response.put("Data", posts);
            return new ResponseEntity<>(response,HttpStatus.OK);
        }

    }

    //SHOW POST BY ID
    @ApiOperation("TO DO: TO SHOW POST BY GROUP ID")
    @GetMapping("/posts/{id}")
    public ResponseEntity<Map<String, Object>> findPostGroupById(@PathVariable("id") int id) {
        Map<String, Object> response = new HashMap<>();
        List<Post> post = postService.showAllPostsByGroup(id);
        if (post == null) {
            response.put("status", false);
            response.put("message", "Post is not found.");
            return new ResponseEntity<>(response,HttpStatus.NOT_ACCEPTABLE);
        } else {
            response.put("status", true);
            response.put("message", "Successfully!");
            response.put("Data", post);
            return new ResponseEntity<>(response,HttpStatus.OK);
        }
    }

    //INSERT
    @PostMapping("/posts")
    @ApiOperation("TO DO: TO INSERT A NEW POST")
    public ResponseEntity<Map<String, Object>> insert(@RequestBody Post post) {
        Map<String, Object> response = new HashMap<>();
        if (postService.insert(post)) {
            response.put("status", true);
            response.put("message", "Post is successfully added!");
            response.put("Data", postService.showPostByGroupId(post.getGroup_id()));
            return new ResponseEntity<>(response,HttpStatus.OK);
        } else {
            response.put("status", false);
            response.put("message", "Failed");
            return new ResponseEntity<>(response,HttpStatus.NOT_ACCEPTABLE);
        }
    }


    //UPDATE
    @ApiOperation("TO DO: TO UPDATE POST BY ID")
    @PutMapping("/posts/{id}")
    public ResponseEntity<Map<String, Object>> editPost(String detail,@PathVariable("id") int id) {
        Map<String, Object> response = new HashMap<>();
        if (postService.editPostById(detail,id)) {
            response.put("status", true);
            response.put("message", "Post is successfully updated!");
            response.put("Data", postService.showPostByGroupId(id));
            return new ResponseEntity<>(response,HttpStatus.OK);
        } else {
            response.put("status", false);
            response.put("message", "Failed to update post.");
            return new ResponseEntity<>(response,HttpStatus.NOT_ACCEPTABLE);
        }
    }

    //DELETE
    @ApiOperation("TO DO: TO DELETE POST BY ID")
    @DeleteMapping("/posts/{id}")
    public ResponseEntity<Map<String, Object>> delete(@PathVariable("id")int id) {
        Map<String, Object> response = new HashMap<>();
        if (postService.deletePostById(id)) {
            response.put("status", true);
            response.put("message", "Deleted Successfully!");
            return new ResponseEntity<>(response, HttpStatus.OK);
        } else {
            response.put("status", false);
            response.put("message", "Deleted Fail.");
            return new ResponseEntity<>(response, HttpStatus.NO_CONTENT);
        }
    }


    //SHOW POST BY USER ID
    @ApiOperation("TO DO: TO SHOW POST BY USER ID")
    @GetMapping("/post/{id}")
    public ResponseEntity<Map<String, Object>> showPostByUserId(@PathVariable("id") int id) {
        Map<String, Object> response = new HashMap<>();
        List<Post> post = postService.showPostByUserId(id);
        if (post == null) {
            response.put("status", false);
            response.put("message", "User is not found.");
            return new ResponseEntity<>(response, HttpStatus.NOT_ACCEPTABLE);
        } else {
            response.put("status", true);
            response.put("message", "Successfully!");
            response.put("Data", post);
            return new ResponseEntity<>(response, HttpStatus.OK);
        }
    }

    //SHOW POST BY GROUP ID AND USER ID
    @ApiOperation("TO DO: TO SHOW POST BY USER ID AND GROUP ID")
    @GetMapping("/post/{group_id}/{user_id}")
    public ResponseEntity<Map<String, Object>> showPostByUserIdGroupId(@PathVariable("group_id") Integer group_id,@PathVariable("user_id") Integer user_id) {
        Map<String, Object> response = new HashMap<>();
        List<Post> post = postService.showPostByUserId(user_id);
        if (post == null) {
            response.put("status", false);
            response.put("message", "User and Group are not found!");
            return new ResponseEntity<>(response, HttpStatus.NOT_ACCEPTABLE);
        } else {
            response.put("status", true);
            response.put("message", "Successfully!");
            response.put("Data", post);
            return new ResponseEntity<>(response, HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/uploadImage", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Map<String, Object>> fileUpload(@RequestParam("file") MultipartFile file){
        Map<String, Object> response = new HashMap<>();
        String fileName = "";
        try {
            fileName = GlobalFunctionHelper.uploaded(file);
        } catch (Exception e) {
            e.printStackTrace();
            response.put("status", false);
            response.put("message", "User is not found.");
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        response.put("status", true);
        response.put("message", "Successfully Retrieved Post!");
        response.put("DATA", fileName);
        return new ResponseEntity<>(response,HttpStatus.OK);
    }
}