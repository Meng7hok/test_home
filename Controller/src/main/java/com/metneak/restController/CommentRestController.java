package com.metneak.restController;

import com.metneak.repository.model.User;
import com.metneak.service.CommentService;
import com.metneak.repository.model.Comment;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/")
@Api(value = "Comment Restful API",tags ={"v2/api/comments"},description = "comment crud")
public class CommentRestController {
    @Autowired
    public CommentService commentService;

    @ApiOperation("TO DO: TO INSERT COMMENT DETAIL BY ID")
    @PostMapping("/comment/new")
    public ResponseEntity<Map<String, Object>> addComment(Comment comment) {
//        Comment cmt =new Comment();
//        cmt.setDetail("lol");
//        cmt.getUser().setId(2);
//        cmt.setPost_id(1);
//        cmt.setParent_id(0);
//        System.out.println(cmt);
        Map<String, Object> response = new HashMap<>();
        if (!commentService.addComment(comment)) {
            response.put("status", false);
            response.put("message", "Insert fail");
            return new ResponseEntity<>(response, HttpStatus.NOT_ACCEPTABLE);
        }else {
            response.put("status", true);
            response.put("message", "Successfully Updated!");
            return new ResponseEntity<>(response,HttpStatus.OK);
        }


    }

    @ApiOperation("TO DO: TO UPDATE COMMENT DETAIL BY ID")
    @PutMapping("/comment/update")
    public ResponseEntity<Map<String, Object>> updateComment(@RequestParam("detail") String detail,@RequestParam("id") Integer id) {
        Map<String, Object> response = new HashMap<>();

        if (!commentService.updateComment(detail, id)) {
            response.put("status", false);
            response.put("message", "update fail");
            return new ResponseEntity<>(response, HttpStatus.NOT_ACCEPTABLE);
        } else {
            response.put("status", true);
            response.put("message", "Successfully Updated!");
            return new ResponseEntity<>(response,HttpStatus.OK);
        }

    }

    @ApiOperation("TO DO: TO REMOVE COMMENT DETAIL BY ID")
    @PostMapping("/comment/remove")
    public ResponseEntity<Map<String, Object>> removeComment(@RequestParam("id") Integer id) {
        Map<String, Object> response = new HashMap<>();

        if (!commentService.removeComment(id)) {
            response.put("status", false);
            response.put("message", "remove fail");
            return new ResponseEntity<>(response, HttpStatus.NOT_ACCEPTABLE);
        } else {
            response.put("status", true);
            response.put("message", "Successfully Removed!");
            return new ResponseEntity<>(response,HttpStatus.OK);
        }

    }
}
