package com.metneak.repository;

public class Session {

   private String code;
   private String csrf;

    public Session() {
    }

    @Override
    public String toString() {
        return "Session{" +
                "code='" + code + '\'' +
                ", csrf='" + csrf + '\'' +
                '}';
    }

    public Session(String code, String csrd) {
        this.code = code;
        this.csrf = csrd;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCsrf() {
        return csrf;
    }

    public void setCsrf(String csrf) {
        this.csrf = csrf;
    }
}
