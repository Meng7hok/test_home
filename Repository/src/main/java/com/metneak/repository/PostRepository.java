package com.metneak.repository;

import com.metneak.repository.model.*;
import com.metneak.repository.provider.PostProvider;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;


@Repository
public interface PostRepository {

    //LIST OF POSTS
    @Select("SELECT p.*,u.* FROM mn_posts p INNER JOIN mn_users u ON u.id=p.user_id WHERE p.group_id = #{group_id} AND u.status=1 ORDER BY p.time DESC")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "user",column = "user_id",many = @Many(select = "showUserById")),
            @Result(property = "comments", column = "id", many = @Many(select = "showCommentById")),
            @Result(property = "likes", column = "id", many = @Many(select = "showLikeById"))
    })
    List<Post> showAllPostsByGroup(@Param("group_id") Integer group_id);

    //LIST OF POSTS
    @Select("SELECT p.*,u.* FROM mn_posts p INNER JOIN mn_users u ON u.id=p.user_id AND u.status=1 ORDER BY p.time DESC")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "user",column = "user_id",many = @Many(select = "showUserById")),
            @Result(property = "comments", column = "id", many = @Many(select = "showCommentById")),
            @Result(property = "likes", column = "id", many = @Many(select = "showLikeById"))
    })
    List<Post> showAllPosts();

    //SHOW USER BY ID
    @Select("SELECT * FROM mn_users WHERE id=#{id} AND status=1")
    @Results({
            @Result(property =  "id",column = "id"),
            @Result(property = "location_id",column = "id"),
            @Result(property = "group",column = "group_id" )
    })
    List<User>showUserById(int id);

    //SHOW POST BY GROUP ID///
    @Select("SELECT * FROM mn_posts WHERE group_id =#{id} AND status=1")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "comments", column = "id", many = @Many(select = "showCommentById")),
            @Result(property = "likes", column = "id", many = @Many(select = "showLikeById"))
    })
    List<Post> showPostByGroupId(int id);



    //SHOW COMMENT OBJECT IN EACH POST'S ID
    @Select("SELECT * FROM mn_comments c INNER JOIN mn_users u ON u.id=c.user_id WHERE c.id=#{c.id} AND c.status=1")
    @Results({
            @Result(property = "id",column = "id"),
            @Result(property = "user",column = "user_id",many = @Many(select = "showUserById"))
    })
    List<Comment> showCommentById(int id);

    //SHOW LIKE OBJECTS IN EACH POST'S ID
    @Select("SELECT * FROM mn_likes l INNER JOIN mn_users u ON u.id=l.user_id WHERE l.id=#{l.id}")
    @Results({
            @Result(property = "id",column = "id"),
            @Result(property = "user",column = "user_id",many = @Many(select = "showUserById"))
    })
    List<Like> showLikeById(int id);


    //UPDATE POST
    @Update("UPDATE mn_posts SET detail=#{detail} WHERE id=#{id} AND status=1")
    boolean editPostById(String detail,int id);


    //INSERT NEW POST
    @Insert("INSERT INTO mn_posts(detail, image, user_id, group_id) VALUES (#{detail}, #{image},#{user.id},#{group_id})")
    @Options(useGeneratedKeys = true,keyProperty = "group_id")
    boolean insert(Post post);

    //DELETE
    @Delete("UPDATE mn_posts SET status=0 WHERE id=#{id}")
    boolean deletePostById(int id);


    //SHOW POST BY USER ID
    @Select("SELECT p.id,p.detail,p.image,p.user_id FROM mn_posts p INNER JOIN mn_users u ON u.id=p.user_id WHERE u.id=#{u.id}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "user",column = "user_id",many = @Many(select = "showUserById")),
            @Result(property = "comments", column = "id", many = @Many(select = "showCommentById")),
            @Result(property = "likes", column = "id", many = @Many(select = "showLikeById"))
    })
    List<Post>showPostByUserId(int id);

    @Transactional
    @SelectProvider(method = "FindPostByUserIdGroupId",type = PostProvider.class)
//    @Select("SELECT p.*,u.*,g.* FROM mn_posts p INNER JOIN mn_users u ON u.id=p.user_id WHERE p.group_id = #{group_id} AND p.user_id=#{user_id} AND u.status=1")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "user",column = "user_id",many = @Many(select = "showUserById")),
            @Result(property = "group", column = "group_id"),
            @Result(property = "comments", column = "id", many = @Many(select = "showCommentById")),
            @Result(property = "likes", column = "id", many = @Many(select = "showLikeById"))
    })
    List<Post>showPostByGroupIdUserId(Integer group_id, int user_id);

}
