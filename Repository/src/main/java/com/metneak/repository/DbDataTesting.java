package com.metneak.repository;

import java.util.List;

import com.metneak.repository.model.Comment;
import com.metneak.repository.model.Like;
import com.metneak.repository.model.Post;

import org.apache.ibatis.annotations.Many;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

//@Repository
public interface DbDataTesting {

//    // @Select("select tester from testing limit 1")
//    // Object list();
//
//    @Select("select * from mn_posts ")
//    @Results({
//        @Result(property = "comments" ,column="id" ,many = @Many(select = "commentById") ),
//        @Result(property = "likes",column = "id", many = @Many(select = "likeById"))
//    })
//    List<Post> posts();
//
//    @Select("select * from mn_comments where post_id = #{id}")
//     List<Comment> commentById(int id);
//
//    @Select("select * from mn_likes where post_id = #{id}")
//    List<Like> likeById(int id);
//
//    @Select("select * from mn_users where id = 1 ")
//    @Results({
//            @Result(property = "group",column = "group_id" )
//    })
//    public com.metneak.repository.model.User findUser();
}
