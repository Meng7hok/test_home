package com.metneak.repository;

import com.metneak.repository.model.School;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SchoolRepository {

    @Select("SELECT * FROM mn_schools")
    @Results({
            @Result(property = "name",column = "school_name")
    })
    List<School> findSchool();
}
