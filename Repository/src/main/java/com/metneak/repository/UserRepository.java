package com.metneak.repository;

import com.metneak.repository.model.Group;
import com.metneak.repository.model.Location;
import com.metneak.repository.model.User;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository {

    @Select("select phone from mn_users where status = 1")
    List<String> allUserNumber();

    @Insert("INSERT INTO public.mn_users(name, phone, location_id, group_id, gender, birthdate, status) " +
            "VALUES ( #{name}, #{phone}, #{location_id}, ARRAY[#{group1},#{group2},#{group3}],#{gender}, #{birthdate}, 1); ")
    boolean addNewUser(User user);

    @Select("SELECT * FROM mn_users WHERE phone = #{phone_number} AND status = 1 LIMIT 1")
    User findUserByPhone(String phone_number);



    //SHOW USER BY ID
    @Select("SELECT * FROM mn_users WHERE id=#{id}")
    @Results({
            @Result(property =  "id",column = "id"),
            @Result(property = "location_id",column = "id"),
            @Result(property = "group",column = "group_id" )
    })
    List<User>showUserById(int id);

    //SHOW LOCATION BY ID
    @Select("SELECT id FROM mn_locations")
    Location showLocationById(int id);

    //SHOW USER_GROUP BY ID
    @Select("SELECT user_id, group_id FROM mn_user_groups WHERE id=#{id}")
    List<Group>showUserGroupById(int id);

}
