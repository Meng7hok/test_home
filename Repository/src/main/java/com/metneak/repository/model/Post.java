package com.metneak.repository.model;

import java.util.List;

/**
 * Post
 */
public class Post {

    int id;
    String detail; 
    String image;
    User user;
    int group_id;
    List<Comment> comments;
    List<Like> likes;

    public Post(){

    }

    @Override
    public String toString() {
        return "Post{" +
                "id=" + id +
                ", detail='" + detail + '\'' +
                ", image='" + image + '\'' +
                ", user=" + user +
                ", group_id=" + group_id +
                ", comments=" + comments +
                ", likes=" + likes +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getGroup_id() {
        return group_id;
    }

    public void setGroup_id(int group_id) {
        this.group_id = group_id;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public List<Like> getLikes() {
        return likes;
    }

    public void setLikes(List<Like> likes) {
        this.likes = likes;
    }

    public Post(String detail, String image, User user, int group_id, List<Comment> comments, List<Like> likes) {
        this.detail = detail;
        this.image = image;
        this.user = user;
        this.group_id = group_id;
        this.comments = comments;
        this.likes = likes;
    }
}