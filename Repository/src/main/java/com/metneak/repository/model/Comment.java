package com.metneak.repository.model;

import java.sql.Date;
import java.util.List;

/**
 * Comment
 */
public class Comment {

    Integer id;
    String detail;
    User user;
    Integer post_id;
    Integer parent_id;
    List<Comment> lists;
    Date time;

    @Override
    public String toString() {
        return "Comment{" +
                "id=" + id +
                ", detail='" + detail + '\'' +
                ", user=" + user +
                ", post_id=" + post_id +
                ", parent_id=" + parent_id +
                ", lists=" + lists +
                ", time=" + time +
                '}';
    }

    public Comment(){

    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Integer getPost_id() {
        return post_id;
    }

    public void setPost_id(Integer post_id) {
        this.post_id = post_id;
    }

    public Integer getParent_id() {
        return parent_id;
    }

    public void setParent_id(Integer parent_id) {
        this.parent_id = parent_id;
    }

    public List<Comment> getLists() {
        return lists;
    }

    public void setLists(List<Comment> lists) {
        this.lists = lists;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Comment(String detail, User user, Integer post_id, Integer parent_id, List<Comment> lists, Date time) {
        this.detail = detail;
        this.user = user;
        this.post_id = post_id;
        this.parent_id = parent_id;
        this.lists = lists;
        this.time = time;
    }
}