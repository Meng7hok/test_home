package com.metneak.repository.model;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

@Repository
public interface CommentRepository {
    //status is no need to insert
    @Insert("INSERT INTO mn_comments(detail,user_id, post_id, parent_id,status) VALUES (#{detail},#{user.id},#{post_id},#{parent_id},1)")
    boolean addComment(Comment comment);

    @Update("UPDATE mn_comments SET detail = #{detail} where id=#{id} ")
    Boolean updateComment(@Param("detail") String detail,@Param("id")Integer id );

    @Update("UPDATE mn_comments SET status = 0 where id=#{id} ")
    Boolean removeComment(@Param("id")Integer id);
}
