package com.metneak.repository.provider;

import com.metneak.repository.model.Post;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;
import org.springframework.web.bind.annotation.PathVariable;

import java.awt.*;

import static org.apache.ibatis.jdbc.SelectBuilder.SQL;

public class PostProvider {

    public String FindPostByUserIdGroupId(@Param("post") Post post, @Param("group_id") Integer[] group_id, @Param("user_id") int user_id){
        return new SQL() {{
            SELECT("p.*,u.*,g.*");
            FROM("mn_posts p");
            INNER_JOIN("mn_users u ON u.id=p.user_id");
            INNER_JOIN("mn_groups g ON g.id=p.group_id");
            if(post.getUser().getId()==0){
                WHERE("p.group_id = #{group_id}");
            }
            if(post.getGroup_id()==0){
                WHERE(" p.user_id=#{user_id}");
                AND();
                WHERE("p.group_id = #{group_id} AND p.status=1");
            }
            ORDER_BY("p.time DESC");
        }}.toString();
    }


}
